//IIFE
(function(){
    //Create an angular module
    var RegApp = angular.module("RegApp", []);
    
    //Create a function consructor to be used as a controller
    var RegCtrl = function($http){
        var regCtrl = this;
        regCtrl.name = "";
        regCtrl.email = "";
        regCtrl.message = "";
        
        regCtrl.clearForm = function(){
            regCtrl.name = "";
            regCtrl.email = "";
            regCtrl.message = "Cleared";
        };
        
        regCtrl.submit = function(){
            console.log("submitted");
        
            var promise = $http.get("http://localhost:3000/register-interest",{ //can just be "/register-interest" based from loaded page
                params:{
                    name: regCtrl.name,
                    email: regCtrl.email
                }
            });
            promise.then(function(result){
                regCtrl.message="Your registration id is " + result.data.regId;
                //regCtrl.message = JSON.stringify(result.data.regId); //Check which part it is in
                }, 
            function(result){
                    regCtrl.message = result.data.regId; //Check which part it is in
                }
            );
        };    
    };

    RegCtrl.$inject = ["$http"];


    //Register function constructor as controller
    RegApp.controller("RegCtrl",RegCtrl);
})();