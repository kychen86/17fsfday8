var express = require("express");

var app = express();
var regEmail=["add@add"];
var regIdVal = "";

app.get("/register-interest",function(req,res){

    var email= req.query.email.toLowerCase();
    console.log(email); //checking
    //check email validity
    var dup = false;
    for(var i in regEmail){
        console.log(regEmail[i]); //checking
        if (regEmail[i]===email){
            dup = true;
            console.log("duplicated");
        }
        else{
            regEmail.push(email);
            for (var j in regEmail){ //checking
                console.log("pass %s",regEmail[j])
            }
            dup = false;           
        }
    }

    //if email is valid 
    if(dup===false){
        regIdVal = Math.round(Math.random()*10000) + regEmail.length;
        console.log(regIdVal); //checking
        res.status(202);
        res.type("application/json");
        res.json({regId:regIdVal});
    }
    //if email is invalid ie duplicated
    else if (dup){
        res.status(400);
        res.type("application/json");
        res.json({regId:"error, can't register a duplicate email"});
    }




});

app.use(express.static(__dirname + "/public"));
app.use("/libs",express.static(__dirname + "/bower_components"));

var port = process.argv[2] || 3000;

app.listen(port, function(){
    console.log("Application started at %d", port);
})